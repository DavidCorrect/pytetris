import pygame
from pygame.locals import *

import random


# Initialize program
pygame.init()
 
# Assign FPS a value
FPS = 30
FramePerSec = pygame.time.Clock()
 
# Setting up color objects
BLUE  = (0, 0, 255)
LBLUE = (0, 191, 255)
DBLUE = (0, 0, 139)
RED   = (255, 0, 0)
GREEN = (0, 255, 0)
INDIGO = (75, 0, 130)
YELLOW = (255, 255, 0)
ORANGE = (255, 140, 0)

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREY = (127, 127, 127)
 
# Setup a display with title
DISPLAYSURF = pygame.display.set_mode((750, 600))
DISPLAYSURF.fill(GREY)
pygame.display.set_caption("Tetris")

# "Constants"
W, H = pygame.display.get_surface().get_size()
Emp = (-1, -1, -1)
Full = 1
TileSize = 25
rows = int(H / TileSize)
cols = int(W / 2 / TileSize)

# Peice Constructions
line = [[Emp, LBLUE, Emp, Emp],
        [Emp, LBLUE, Emp, Emp],
        [Emp, LBLUE, Emp, Emp],
        [Emp, LBLUE, Emp, Emp]]

rL =   [[Emp, ORANGE, ORANGE, Emp],
        [Emp, ORANGE, Emp, Emp],
        [Emp, ORANGE, Emp, Emp],
        [Emp, Emp, Emp, Emp]]

lL =   [[Emp, DBLUE, DBLUE, Emp],
        [Emp, Emp, DBLUE, Emp],
        [Emp, Emp, DBLUE, Emp],
        [Emp, Emp, Emp, Emp]]

box =  [[Emp, Emp, Emp, Emp],
        [Emp, YELLOW, YELLOW, Emp],
        [Emp, YELLOW, YELLOW, Emp],
        [Emp, Emp, Emp, Emp]]

rZ =   [[Emp, Emp, GREEN, Emp],
        [Emp, GREEN, GREEN, Emp],
        [Emp, GREEN, Emp, Emp],
        [Emp, Emp, Emp, Emp]]

lZ =   [[Emp, RED, Emp, Emp],
        [Emp, RED, RED, Emp],
        [Emp, Emp, RED, Emp],
        [Emp, Emp, Emp, Emp]]

t =    [[Emp, Emp, INDIGO, Emp],
        [Emp, INDIGO, INDIGO, Emp],
        [Emp, Emp, INDIGO, Emp],
        [Emp, Emp, Emp, Emp]]

peices = [line, rL, lL, box, rZ, lZ, t]

# Classes and functions
class PlayArea:
    def __init__(self):
        self.arr = [[Emp for i in range(cols)] for j in range(rows)]
    
    def changeTile(self, x, y, val):
        self.arr[int(y)][int(x)] = val
    
    def drawPlayArea (self):
        for y in range(len(self.arr)):
            for x in range(len(self.arr[y])):
                if (self.arr[y][x] != Emp):
                    pygame.draw.rect(DISPLAYSURF, self.arr[y][x], (int(x * TileSize + W / 4), y * TileSize, TileSize, TileSize))
                    # rect = (Display, Color, 4-Tuple(X, Y, Length, Heighth))


class Piece:
    def __init__(self, peice):
        self.arr = peice
        self.x = 6
        self.y = 0
        self.speed = 4
        self.counter = 0
    
    def rotate(self):
        self.arr = zip(*self.arr[::-1])

    def draw (self, pa):
        for y in range(len(self.arr)):
            for x in range(len(self.arr[y])):
                if(self.arr[y][x] != Emp):
                    pa.changeTile(x + self.x, y + self.y, self.arr[y][x])

    def update(self, pa):
        done = False

        keyPress = pygame.key.get_pressed()

        for y in range(len(self.arr)):
            for x in range(len(self.arr[y])):
                if(self.arr[y][x] != Emp):
                    pa.arr[self.y + y][self.x + x] = Emp

        if(keyPress[K_LEFT] and self.checkXBounds(-1)):
            self.x -= 1
        elif(keyPress[K_RIGHT] and self.checkXBounds(1)):
            self.x += 1
        elif(keyPress[K_SPACE]):
            self.rotate()

        if(self.checkYBounds(pa)):
            if(self.counter % self.speed == 0):
                self.y += 1
                
            self.counter += 1
        else:
            done = True
        
        self.draw(pa)

        return done

    def checkYBounds(self, pa):
        for y in range(len(self.arr)):
            for x in range(len(self.arr[y])):
                if(self.arr[y][x] != Emp): # Is actually a tile of peice
                    if(y+1 < int(len(self.arr))): # y+1 is in the range of peice bounding box
                        if(self.arr[y+1][x] == Emp): # There's not another bit of the peice below
                            if(self.y + y+1 < rows): # y+1 is still on board
                                if(pa.arr[self.y + y+1][self.x + x] != Emp): # There's existing blocks below
                                    return False
                            else:
                                return False
                    elif(self.y + y+1 < rows): # y+1 is still on board
                        if (pa.arr[self.y + y+1][self.x + x] != Emp): # There's a block below a bottom row bit
                            return False
                    else:
                        return False
        return True

    def checkXBounds(self, dir):
        for y in range(len(self.arr)):
            for x in range(len(self.arr[y])):
                if (dir > 0):
                    if(self.x + x + dir > cols+1):
                        return False
                else:
                    if(self.x + x + dir < -1):
                        return False
                
                if(self.arr[y][x] != Emp): # Is actually a tile of peice
                    if(x+dir < int(len(self.arr[0]))): # x+1 is in the range of peice bounding box
                        if(self.arr[y][x+dir] == Emp): # There's not another bit to the right
                            if(self.x + x+dir < cols): # x+1 is still on board
                                if(pa.arr[self.y + y][self.x + x+dir] != Emp): # There's blocks to right
                                    return False
                            else:
                                return False
        return True


    """
    def rotateLeft(self, dir):
        if(dir == 'L'):
            self.arr = zip(*self.arr)   # not sure if this works
            self.arr = self.arr[::-1]
        elif(dir == 'R'):
            self.arr = zip(*self.arr[::-1]) # this should work fine
    """

        

def drawBack():
    # Draw Background
    DISPLAYSURF.fill(GREY)
    pygame.draw.rect(DISPLAYSURF, BLACK, (0, 0, int(W / 4), H))
    pygame.draw.rect(DISPLAYSURF, BLACK, (int(W / 4 * 3), 0, int(W / 4), H))

    # Draw display grid (Play area is middle half)
    for i in range(int(H / TileSize)):
        pygame.draw.line(DISPLAYSURF, WHITE, (int(W / 4), i * TileSize), (int(W / 4 * 3), i * TileSize))
    
    for i in range(int(W / 2 / TileSize)):
        pygame.draw.line(DISPLAYSURF, WHITE, (int(W / 4 + i * TileSize), 0), (int(W / 4 + i * TileSize), H))


pa = PlayArea()

# Test Code
couter = 0

pa.changeTile(6, 15, BLUE)
testPeice = Piece(line)

# Game Loop
while True:
    pygame.display.update()
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    drawBack()

    
    if(testPeice.update(pa)):
        testPeice = Piece(random.choice(peices))
    
    pa.drawPlayArea()
    
    FramePerSec.tick(FPS)